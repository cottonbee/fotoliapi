# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'fotoliapi/version'

Gem::Specification.new do |spec|
  spec.name          = 'fotoliapi'
  spec.version       = Fotoliapi::VERSION
  spec.authors       = ['Marcin Wąsowski', 'Paweł Buchowski']
  spec.email         = ['marcin@refactory.net', 'pawel.buchowski@refactory.net']
  spec.summary       = 'Fotolia API client library'
  spec.description   = 'Fotolia API client library for rails'
  spec.homepage      = 'http://refactory.net'
  spec.license       = 'MIT'

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ['lib']



  spec.add_development_dependency 'bundler', '~> 1.7'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec', '~> 3.3'

  spec.add_dependency 'faraday', '~> 0.9'

end
