require 'YAML'
require 'spec_helper'

describe Fotoliapi::Api do
  before(:each) do
    yaml_hash = YAML.load_file(File.dirname(__FILE__) + '/fotolia.yml')
    yaml_hash.keys.each do |key|
      yaml_hash[(begin
                   key.to_sym
                 rescue
                   key
                 end) || key] = yaml_hash.delete(key)
    end
    Fotoliapi.configure do |c|
      c.from_h(yaml_hash)
    end
  end

  it 'tests no_api_key_response' do
    f_api = Fotoliapi::Api.new(nil)
    response = f_api.get_categories_2
    expect(Fotoliapi::Models::AnswerErrorFotolia).to eq response.class
    expect(401).to eq response.code
  end

  it 'tests get_error_type' do
    key = 'dfdfdfg'
    f_api = Fotoliapi::Api.new(key)
    response = f_api.test_get_error
    expect(Fotoliapi::Models::AnswerErrorFotolia).to eq response.class
  end

  it 'tests get_api_key_return' do
    key = 'fdfdsfdsfsd'
    f_api = Fotoliapi::Api.new(key)
    expect(key).to eq f_api.get_api_key
  end

  # SEARCH

  it 'tests get_search_results_type' do
    key = 'fdfdsfdsfsd'
    f_api = Fotoliapi::Api.new(key)
    response = f_api.get_search_results(nil, nil)
    expect(Fotoliapi::Models::AnswerGetSearchResultsFotolia).to eq response.class
  end

  it 'tests get_categories_1_type' do
    key = 'fdfdsfdsfsd'
    f_api = Fotoliapi::Api.new(key)
    response = f_api.get_categories_1
    expect(Fotoliapi::Models::AnswerGetCategories1Fotolia).to eq response.class
  end

  it 'tests get_categories_2_type' do
    key = 'fdfdsfdsfsd'
    f_api = Fotoliapi::Api.new(key)
    response = f_api.get_categories_2
    expect(Fotoliapi::Models::AnswerGetCategories2Fotolia).to eq response.class
  end

  it 'tests get_tags_type' do
    key = 'fdfdsfdsfsd'
    f_api = Fotoliapi::Api.new(key)
    response = f_api.get_tags
    expect(Fotoliapi::Models::AnswerGetTagsFotolia).to eq response.class
  end

  it 'tests get_galleries_type' do
    key = 'fdfdsfdsfsd'
    f_api = Fotoliapi::Api.new(key)
    response = f_api.get_galleries
    expect(Fotoliapi::Models::AnswerGetGalleriesFotolia).to eq response.class
  end

  it 'tests get_seasonal_galleries_type' do
    key = 'fdfdsfdsfsd'
    f_api = Fotoliapi::Api.new(key)
    response = f_api.get_seasonal_galleries
    expect(Fotoliapi::Models::AnswerGetSeasonalGalleriesFotolia).to eq response.class
  end

  it 'tests get_countries_type' do
    key = 'fdfdsfdsfsd'
    f_api = Fotoliapi::Api.new(key)
    response = f_api.get_countries
    expect(Fotoliapi::Models::AnswerGetCountriesFotolia).to eq response.class
  end

  # MEDIA

  it 'tests get_media_data_type' do
    key = 'fdfdsfdsfsd'
    f_api = Fotoliapi::Api.new(key)
    response = f_api.get_media_data 1
    expect(Fotoliapi::Models::AnswerGetMediaDataFotolia).to eq response.class
  end

  it 'tests get_bulk_media_data_type' do
    key = 'fdfdsfdsfsd'
    f_api = Fotoliapi::Api.new(key)
    response = f_api.get_bulk_media_data 1
    expect(Fotoliapi::Models::AnswerGetBulkMediaDataFotolia).to eq response.class
  end

  it 'tests get_media_galleries_type' do
    key = 'fdfdsfdsfsd'
    f_api = Fotoliapi::Api.new(key)
    response = f_api.get_media_galleries 1
    expect(Fotoliapi::Models::AnswerGetMediaGalleriesFotolia).to eq response.class
  end

  it 'tests get_media_type' do
    key = 'fdfdsfdsfsd'
    f_api = Fotoliapi::Api.new(key)
    response = f_api.get_media 1, 'test'
    expect(Fotoliapi::Models::AnswerGetMediaFotolia).to eq response.class
  end

  # USER

  it 'tests login_user_type' do
    key = 'fdfdsfdsfsd'
    f_api = Fotoliapi::Api.new(key)
    response = f_api.login_user 'login', 'pass'
    expect(Fotoliapi::Models::AnswerLoginUserFotolia).to eq response.class
  end

  it 'tests logout_user_type' do
    key = 'fdfdsfdsfsd'
    f_api = Fotoliapi::Api.new(key)
    response = f_api.logout_user
    expect(TrueClass).to eq response.class
  end

  it 'tests sign_up_type' do
    key = 'fdfdsfdsfsd'
    f_api = Fotoliapi::Api.new(key)
    h = {}
    h[:login] = ''
    h[:password] = ''
    h[:email] = ''
    h[:language_id] = ''
    response = f_api.sign_up h
    expect(Fotoliapi::Models::AnswerUserSignUpFotolia).to eq response.class
  end

  it 'tests get_sales_data_type' do
    key = 'fdfdsfdsfsd'
    f_api = Fotoliapi::Api.new(key)
    response = f_api.get_sales_data
    expect(Fotoliapi::Models::AnswerGetSalesDataFotolia).to eq response.class
  end

  it 'tests get_user_advanced_stats_type' do
    key = 'fdfdsfdsfsd'
    f_api = Fotoliapi::Api.new(key)
    response = f_api.get_user_advanced_stats 'tt', 'day'
    expect(Fotoliapi::Models::AnswerGetUserAdvancedStatsFotolia).to eq response.class
  end

  it 'tests get_user_stats_type' do
    key = 'fdfdsfdsfsd'
    f_api = Fotoliapi::Api.new(key)
    response = f_api.get_user_stats
    expect(Fotoliapi::Models::AnswerGetUserStatsFotolia).to eq response.class
  end

  it 'tests delete_user_gallery' do
    key = 'fdfdsfdsfsd'
    f_api = Fotoliapi::Api.new(key)
    response = f_api.delete_user_gallery 12
    expect(Fotoliapi::Models::AnswerDeleteUserGalleryFotolia).to eq response.class
  end

  it 'tests create_user_gallery_type' do
    key = 'fdfdsfdsfsd'
    f_api = Fotoliapi::Api.new(key)
    response = f_api.create_user_gallery 'test'
    expect(Fotoliapi::Models::AnswerCreateUserGalleryFotolia).to eq response.class
  end

  it 'tests add_to_user_gallery_type' do
    key = 'fdfdsfdsfsd'
    f_api = Fotoliapi::Api.new(key)
    response = f_api.add_to_user_gallery 12, 12
    expect(Fotoliapi::Models::AnswerAddToUserGalleryFotolia).to eq response.class
  end

  it 'tests remove_from_user_gallery_type' do
    key = 'fdfdsfdsfsd'
    f_api = Fotoliapi::Api.new(key)
    response = f_api.remove_from_user_gallery 12, 12
    expect(Fotoliapi::Models::AnswerRemoveFromUserGalleryFotolia).to eq response.class
  end

  it 'tests get_user_gallery_medias_type' do
    key = 'fdfdsfdsfsd'
    f_api = Fotoliapi::Api.new(key)
    response = f_api.get_user_gallery_medias
    expect(Fotoliapi::Models::AnswerGetUserGalleryMediasFotolia).to eq response.class
  end

  it 'tests get_user_galleries_type' do
    key = 'fdfdsfdsfsd'
    f_api = Fotoliapi::Api.new(key)
    response = f_api.get_user_galleries
    expect(Fotoliapi::Models::AnswerGetUserGalleriesFotolia).to eq response.class
  end

  it 'tests move_up_media_in_user_gallery' do
    key = 'fdfdsfdsfsd'
    f_api = Fotoliapi::Api.new(key)
    response = f_api.move_up_media_in_user_gallery 12
    expect(Fotoliapi::Models::AnswerMoveUpMediaInUserGalleryFotolia).to eq response.class
  end

  it 'tests move_down_media_in_user_gallery_gallery' do
    key = 'fdfdsfdsfsd'
    f_api = Fotoliapi::Api.new(key)
    response = f_api.move_down_media_in_user_gallery 12
    expect(Fotoliapi::Models::AnswerMoveDownMediaInUserGalleryFotolia).to eq response.class
  end

  it 'tests move_media_to_top_in_user_gallery_gallery' do
    key = 'fdfdsfdsfsd'
    f_api = Fotoliapi::Api.new(key)
    response = f_api.move_media_to_top_in_user_gallery 12
    expect(Fotoliapi::Models::AnswerMoveMediaToTopInUserGalleryFotolia).to eq response.class
  end

  it 'tests get_user_advanced_stats' do
    key = 'fdfdsfdsfsd'
    f_api = Fotoliapi::Api.new key
    response = f_api.get_user_advanced_stats 'member_downloaded_photos', 'month', 'one_day'
    expect(Fotoliapi::Models::AnswerGetUserAdvancedStatsFotolia).to eq response.class
  end

  it 'tests get_last_online_contents' do
    key = 'fdfdsfdsfsd'
    f_api = Fotoliapi::Api.new key
    response = f_api.get_last_online_contents 0, '2015-07-15', '2015-07-23', 110
    expect(Fotoliapi::Models::AnswerGetLastOnlineContentsFotolia).to eq response.class
  end

  it 'tests get_upload_folders' do
    key = 'fdfdsfdsfsd'
    f_api = Fotoliapi::Api.new key
    response = f_api.get_upload_folders 0
    expect(Fotoliapi::Models::AnswerGetUploadFoldersFotolia).to eq response.class
  end

  it 'tests get_upload_folders_file_ids' do
    key = 'fdfdsfdsfsd'
    f_api = Fotoliapi::Api.new key
    response = f_api.get_upload_folder_file_ids 1
    expect(Fotoliapi::Models::AnswerGetUploadFolderFileIdsFotolia).to eq response.class
  end

  it 'tests get_last_uploaded_media' do
    key = 'fdfdsfdsfsd'
    f_api = Fotoliapi::Api.new key
    response = f_api.get_last_uploaded_media
    expect(Fotoliapi::Models::AnswerGetLastUploadedMediaFotolia).to eq response.class
  end

  # SHOPPING CART

  it 'tests shopping_cart_get_list' do
    key = 'fdfdsfdsfsd'
    f_api = Fotoliapi::Api.new key
    response = f_api.shopping_cart_get_list
    expect(Fotoliapi::Models::AnswerShoppingCartGetListFotolia).to eq response.class
  end

  it 'tests shopping_cart_add' do
    key = 'fdfdsfdsfsd'
    f_api = Fotoliapi::Api.new key
    response = f_api.shopping_cart_add 1, 'M'
    expect(Fotoliapi::Models::AnswerShoppingCartAddFotolia).to eq response.class
  end

  it 'tests shopping_cart_update' do
    key = 'fdfdsfdsfsd'
    f_api = Fotoliapi::Api.new key
    response = f_api.shopping_cart_update 1, 'M'
    expect(Fotoliapi::Models::AnswerShoppingCartUpdateFotolia).to eq response.class
  end

  it 'tests shopping_cart_transfer_to_lightbox' do
    key = 'fdfdsfdsfsd'
    f_api = Fotoliapi::Api.new key
    response = f_api.shopping_cart_transfer_to_lightbox 1
    expect(Fotoliapi::Models::AnswerShoppingCartTransferToLightboxFotolia).to eq response.class
  end

  it 'tests shopping_cart_clear' do
    key = 'fdfdsfdsfsd'
    f_api = Fotoliapi::Api.new key
    response = f_api.shopping_cart_clear
    expect(Fotoliapi::Models::AnswerShoppingCartClearFotolia).to eq response.class
  end
end
