module Fotoliapi
  module Models
    class SalesDataFotolia
      attr_accessor :id, :license, :sale_type, :sale_date, :commission, :withholdings
      def initialize(id, license, sale_type, sale_date, commission, withholdings)
        @id = id
        @license = license
        @sale_type = sale_type
        @sale_date = sale_date
        @commission = commission
        @withholdings = withholdings
      end

      def self.from_json(json)
        json = JSON.parse json if json.is_a?(String)
        answer = Fotoliapi::Models::SalesDataFotolia.new(json['id'], json['license'], json['sale_type'], json['sale_date'], json['commission'], json['withholdings'])
        answer
      end
    end
  end
end
