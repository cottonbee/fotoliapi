module Fotoliapi
  module Models
    class AnswerShoppingCartGetListFotolia
      attr_accessor :nb_contents, :contents

      def initialize(nb_contents = nil, contents = nil)
        @nb_contents = nb_contents
        @contents = contents
      end

      def self.from_json(json)
        json = JSON.parse json if json.is_a?(String)
        answer = Fotoliapi::Models::AnswerShoppingCartGetListFotolia.new
        answer.nb_contents = json['nb_contents']
        answer.contents = json['contents']
        answer
      end
    end
  end
end
