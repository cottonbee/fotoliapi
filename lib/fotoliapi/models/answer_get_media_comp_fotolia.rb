module Fotoliapi
  module Models
    class AnswerGetMediaCompFotolia
      attr_accessor :url, :width, :height

      def initialize(url = nil, width = nil, height = nil)
        @url = url
        @width = width
        @height = height
      end

      def self.from_json(json)
        json = JSON.parse json if json.is_a?(String)
        answer = Fotoliapi::Models::AnswerGetMediaCompFotolia.new(json['url'], json['width'], json['height'])
        answer
      end
    end
  end
end
