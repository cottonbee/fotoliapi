module Fotoliapi
  module Models
    class LicenseDetailsFotolia
      attr_accessor :size, :ratio, :width, :height, :dpi, :phrase

      def initialize(size = nil, ratio = nil, width = nil, height = nil, dpi = nil, phrase = nil)
        @size = size
        @ratio = ratio
        @width = width
        @height = height
        @dpi = dpi
        @phrase = phrase
      end

      def self.from_json(json)
        json = JSON.parse json if json.is_a?(String)
        answer = Fotoliapi::Models::LicenseDetailsFotolia.new(nil, json['ratio'], json['width'], json['height'], json['dpi'], json['phrase'])
        answer
      end
    end
  end
end
