module Fotoliapi
  module Models
    class LicenseFotolia
      attr_accessor :name, :price

      def initialize(name, price)
        @name = name
        @price = price
      end

      def self.from_json(json)
        json = JSON.parse json if json.is_a?(String)
        answer = Fotoliapi::Models::LicenseFotolia.new(json['name'], json['price'])
        answer
      end
    end
  end
end
