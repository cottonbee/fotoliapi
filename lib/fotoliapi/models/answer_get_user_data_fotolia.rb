module Fotoliapi
  module Models
    class AnswerGetUserDataFotolia
      attr_accessor :id, :language_id, :language_name, :nb_credits, :credit_value, :currency_name, :currency_symbol

      def initialize(id = nil, language_id = nil, language_name = nil, nb_credits = nil, credit_value = nil, currency_name = nil, currency_symbol = nil)
        @id = id
        @language_id = language_id
        @language_name = language_name
        @nb_credits = nb_credits
        @credit_value = credit_value
        @currency_name = currency_name
        @currency_symbol = currency_symbol
      end

      def self.from_json(json)
        json = JSON.parse json if json.is_a?(String)
        answer = Fotoliapi::Models::AnswerGetUserDataFotolia.new(
          json['id'],
          json['language_id'], json['language_name'], json['nb_credits'], json['credit_value'],
          json['currency_name'], json['currency_symbol'])
        answer
      end
    end
  end
end
