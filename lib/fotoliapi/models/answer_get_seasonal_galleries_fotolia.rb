module Fotoliapi
  module Models
    class AnswerGetSeasonalGalleriesFotolia
      attr_accessor :results

      def initialize(results)
        @results = results
      end

      def self.from_json(json)
        json = JSON.parse json if json.is_a?(String)
        results = []
        json.each do |sg|
          seasonal_gallery = Fotoliapi::Models::SeasonalGalleriesItemFotolia.from_json(sg)
          results.push seasonal_gallery
        end
        answer = Fotoliapi::Models::AnswerGetSeasonalGalleriesFotolia.new(results)
        answer
      end
    end
  end
end
