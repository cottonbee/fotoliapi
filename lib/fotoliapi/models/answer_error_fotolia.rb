module Fotoliapi
  module Models
    class AnswerErrorFotolia
      attr_accessor :error, :code

      def initialize(error = nil, code = nil)
        @error = error
        @code = code
      end

      def self.from_json(json)
        json = JSON.parse json if json.is_a?(String)
        error = Fotoliapi::Models::AnswerErrorFotolia.new(json['error'], json['code'])
        error
      end
    end
  end
end
