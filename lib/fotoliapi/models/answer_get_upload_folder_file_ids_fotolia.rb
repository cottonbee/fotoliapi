module Fotoliapi
  module Models
    class AnswerGetUploadFolderFileIdsFotolia
      attr_accessor :results
      def initialize(results)
        @results = results
      end

      def self.from_json(json)
        json = JSON.parse json if json.is_a?(String)
        answer = Fotoliapi::Models::AnswerGetUploadFolderFileIdsFotolia.new(json)
        answer
      end
    end
  end
end
