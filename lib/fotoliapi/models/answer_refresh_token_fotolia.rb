module Fotoliapi
  module Models
    class AnswerRefreshTokenFotolia
      attr_accessor :userrefreshtoken

      def initialize(userrefreshtoken)
        @userrefreshtoken = userrefreshtoken
      end

      def self.from_json(json)
        json = JSON.parse json if json.is_a?(String)
        answer = Fotoliapi::Models::AnswerRefreshTokenFotolia.new(json['user/refreshToken'])
        answer
      end
    end
  end
end
