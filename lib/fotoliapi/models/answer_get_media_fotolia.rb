module Fotoliapi
  module Models
    class AnswerGetMediaFotolia
      attr_accessor :url, :name, :extension

      def initialize(url = nil, name = nil, extension = nil)
        @url = url
        @name = name
        @extension = extension
      end

      def self.from_json(json)
        json = JSON.parse json if json.is_a?(String)
        answer = Fotoliapi::Models::AnswerGetMediaFotolia.new(json['url'], json['name'], json['extension'])
        answer
      end
    end
  end
end
