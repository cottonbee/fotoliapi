module Fotoliapi
  module Models
    class UserGalleryFotolia
      attr_accessor :nb_media, :id, :name

      def initialize(nb_media = nil, id = nil, name = nil)
        @nb_media = nb_media
        @id = id
        @name = name
      end

      def self.from_json(json)
        json = JSON.parse json if json.is_a?(String)
        answer = Fotoliapi::Models::UserGalleryFotolia.new(json['nb_media'], json['id'], json['name'])
        answer
      end
    end
  end
end
