module Fotoliapi
  module Models
    class AnswerRemoveFromUserGalleryFotolia
      attr_accessor :removefromusergallery

      def initialize(removefromusergallery)
        @removefromusergallery = removefromusergallery
      end

      def self.from_json(json)
        json = JSON.parse json if json.is_a?(String)
        answer = Fotoliapi::Models::AnswerRemoveFromUserGalleryFotolia.new(json['removefromusergallery'])
        answer
      end
    end
  end
end
