module Fotoliapi
  module Models
    class AnswerGetUserGalleriesFotolia
      attr_accessor :results

      def initialize(results)
        @results = results
      end

      def self.from_json(json)
        json = JSON.parse json if json.is_a?(String)
        results = []
        json.each do |g|
          results.push Fotoliapi::Models::UserGalleryFotolia.from_json(g)
        end
        answer = Fotoliapi::Models::AnswerGetUserGalleriesFotolia.new(results)
        answer
      end
    end
  end
end
