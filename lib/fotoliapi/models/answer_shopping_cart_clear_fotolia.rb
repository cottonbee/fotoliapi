module Fotoliapi
  module Models
    class AnswerShoppingCartClearFotolia
      attr_accessor :results

      def initialize(results = false)
        @results = results
      end

      def self.from_json(_json)
        answer = Fotoliapi::Models::AnswerShoppingCartClearFotolia.new true
        answer
      end
    end
  end
end
