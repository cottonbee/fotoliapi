module Fotoliapi
  module Models
    class AnswerGetCategories1Fotolia
      attr_accessor :results

      def initialize(results = nil)
        @results = results
      end

      def self.from_json(json)
        json = JSON.parse json if json.is_a?(String)
        results = []
        json.each do |k, h|
          results.push Fotoliapi::Models::CategoryFotolia.new(k, h['id'], h['name'])
        end
        answer = Fotoliapi::Models::AnswerGetCategories1Fotolia.new(results)
        answer
      end
    end
  end
end
