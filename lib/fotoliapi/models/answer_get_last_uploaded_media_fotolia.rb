module Fotoliapi
  module Models
    class AnswerGetLastUploadedMediaFotolia
      attr_accessor :results
      def initialize(results)
        @results = results
      end

      def self.from_json(json)
        json = JSON.parse json if json.is_a?(String)
        answer = Fotoliapi::Models::AnswerGetLastUploadedMediaFotolia.new(json)
        answer
      end
    end
  end
end
