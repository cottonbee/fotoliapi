module Fotoliapi
  module Models
    class AnswerCreateUserGalleryFotolia
      attr_accessor :id

      def initialize(id)
        @id = id
      end

      def self.from_json(json)
        json = JSON.parse json if json.is_a?(String)
        answer = Fotoliapi::Models::AnswerCreateUserGalleryFotolia.new(json['id'])
        answer
      end
    end
  end
end
