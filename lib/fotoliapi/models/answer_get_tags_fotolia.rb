module Fotoliapi
  module Models
    class AnswerGetTagsFotolia
      attr_accessor :results

      def initialize(results = nil)
        @results = results
      end

      def self.from_json(json)
        json = JSON.parse json if json.is_a?(String)
        results = []
        json.each do |h|
          results.push Fotoliapi::Models::TagFotolia.new(h['tag'], h['popularity'])
        end
        answer = Fotoliapi::Models::AnswerGetTagsFotolia.new(results)
        answer
      end
    end
  end
end
