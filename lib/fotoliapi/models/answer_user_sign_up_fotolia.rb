module Fotoliapi
  module Models
    class AnswerUserSignUpFotolia
      attr_accessor :id
      def initialize(id)
        @id = id
      end

      def self.from_json(json)
        answer = Fotoliapi::Models::AnswerUserSignUpFotolia.new(json)
        answer
      end
    end
  end
end
