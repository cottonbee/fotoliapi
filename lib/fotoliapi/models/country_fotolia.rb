module Fotoliapi
  module Models
    class CountryFotolia
      attr_accessor :id, :name

      def initialize(id, name)
        @id = id
        @name = name
      end

      def self.from_json(json)
        json = JSON.parse json if json.is_a?(String)
        answer = Fotoliapi::Models::CountryFotolia.new(json['id'], json['name'])
        answer
      end
    end
  end
end
