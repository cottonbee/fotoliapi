module Fotoliapi
  module Models
    class KeywordFotolia
      attr_accessor :name

      def initialize(name)
        @name = name
      end

      def self.from_json(json)
        json = JSON.parse json if json.is_a?(String)
        answer = Fotoliapi::Models::KeywordFotolia.new(json['name'])
        answer
      end
    end
  end
end
