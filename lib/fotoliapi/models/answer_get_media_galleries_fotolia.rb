module Fotoliapi
  module Models
    class AnswerGetMediaGalleriesFotolia
      attr_accessor :results

      def initialize(results)
        @results = results
      end

      def self.from_json(json)
        json = JSON.parse json if json.is_a?(String)
        results = []
        json.each do |_h, g|
          gallery = Fotoliapi::Models::GalleryFotolia.from_json(g)
          results.push gallery
        end
        answer = Fotoliapi::Models::AnswerGetMediaGalleriesFotolia.new(results)
        answer
      end
    end
  end
end
