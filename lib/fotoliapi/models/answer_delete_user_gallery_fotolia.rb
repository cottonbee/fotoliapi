module Fotoliapi
  module Models
    class AnswerDeleteUserGalleryFotolia
      attr_accessor :deleteusergallery
      def initialize(deleteusergallery)
        @deleteusergallery = deleteusergallery
      end

      def self.from_json(json)
        json = JSON.parse json if json.is_a?(String)
        answer = Fotoliapi::Models::AnswerDeleteUserGalleryFotolia.new(json['deleteusergallery'])
        answer
      end
    end
  end
end
