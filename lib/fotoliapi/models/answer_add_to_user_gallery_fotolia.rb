module Fotoliapi
  module Models
    class AnswerAddToUserGalleryFotolia
      attr_accessor :addtousergallery
      def initialize(addtousergallery)
        @addtousergallery = addtousergallery
      end

      def self.from_json(json)
        json = JSON.parse json if json.is_a?(String)
        answer = Fotoliapi::Models::AnswerAddToUserGalleryFotolia.new(json['addtousergallery'])
        answer
      end
    end
  end
end
