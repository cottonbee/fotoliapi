
module Fotoliapi
  module Models
    class AnswerGetSearchResultsFotolia
      attr_accessor :nb_results, :results

      def initialize(nb_results = nil, results = nil)
        @nb_results = nb_results
        @results = results
      end

      def self.from_json(json)
        json = JSON.parse json if json.is_a?(String)

        answer = Fotoliapi::Models::AnswerGetSearchResultsFotolia.new
        answer.nb_results = json.delete('nb_results')
        answer.results = []
        json.each do |k, h|
          next unless h.is_a?(Hash)
          item = Fotoliapi::Models::SearchResultItemFotolia.from_json(h)
          item.result_no = k
          answer.results.push item
        end
        answer
      end
    end
  end
end
