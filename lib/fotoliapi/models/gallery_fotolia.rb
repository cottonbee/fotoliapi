module Fotoliapi
  module Models
    class GalleryFotolia
      attr_accessor :nb_media, :id, :name, :thumbnail_html_tag, :thumbnail_url, :thumbnail_width, :thumbnail_height

      def initialize(nb_media = nil, id = nil, name = nil,
                     thumbnail_html_tag = nil, thumbnail_url = nil, thumbnail_width = nil, thumbnail_height = nil)
        @nb_media = nb_media
        @id = id
        @name = name
        @thumbnail_html_tag = thumbnail_html_tag
        @thumbnail_url = thumbnail_url
        @thumbnail_width = thumbnail_width
        @thumbnail_height = thumbnail_height
      end

      def self.from_json(json)
        json = JSON.parse json if json.is_a?(String)
        answer = Fotoliapi::Models::GalleryFotolia.new
        answer.nb_media = json['nb_media']
        answer.id = json['id']
        answer.name = json['name']
        answer.thumbnail_html_tag = json['thumbnail_html_tag']
        answer.thumbnail_url = json['thumbnail_url']
        answer.thumbnail_width = json['thumbnail_width']
        answer.thumbnail_height = json['thumbnail_height']
        answer
      end
    end
  end
end
