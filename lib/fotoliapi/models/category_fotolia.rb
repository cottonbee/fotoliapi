module Fotoliapi
  module Models
    class CategoryFotolia
      attr_accessor :result_no, :id, :name

      def initialize(result_no, id, name)
        @result_no = result_no
        @id = id
        @name = name
      end

      def self.from_json(json)
        json = JSON.parse json if json.is_a?(String)
        answer = Fotoliapi::Models::CategoryFotolia.new(nil, json['id'], json['name'])
        answer
      end
    end
  end
end
