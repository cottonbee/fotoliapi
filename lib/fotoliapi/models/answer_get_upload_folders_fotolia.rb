module Fotoliapi
  module Models
    class AnswerGetUploadFoldersFotolia
      attr_accessor :results
      def initialize(results)
        @results = results
      end

      def self.from_json(json)
        json = JSON.parse json if json.is_a?(String)
        results = []
        json.each do |uf|
          results.push Fotoliapi::Models::UploadFolderFotolia.from_json(uf)
        end
        answer = Fotoliapi::Models::AnswerGetUploadFoldersFotolia.new(results)
        answer
      end
    end
  end
end
