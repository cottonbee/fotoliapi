module Fotoliapi
  module Models
    class UploadFolderFotolia
      attr_accessor :id, :url, :name, :nb_files

      def initialize(id, url, name, nb_files)
        @id = id
        @url = url
        @name = name
        @nb_files = nb_files
      end

      def self.from_json(json)
        json = JSON.parse json if json.is_a?(String)
        answer = Fotoliapi::Models::UploadFolderFotolia.new(json['id'], json['url'], json['name'], json['nb_files'])
        answer
      end
    end
  end
end
