module Fotoliapi
  module Models
    class AnswerGetMediaDataFotolia
      attr_accessor :nb_result, :id, :title, :creator_name, :creator_id, :media_type_id,
                    :thumbnail_url, :thumbnail_width, :thumbnail_height, :thumbnail_html_tag,
                    :country_id, :country_name, :nb_views, :nb_downloads,
                    :keywords, # Array<Keyword>
                    :licenses, # Array<License>
                    :licenses_details, # Hash<String, LicenseDetail>
                    :cat1, # Category
                    :cat_hierarchy # Array<Category>

      def initialize(nb_result = nil, id, title, creator_name, creator_id, media_type_id,
                     thumbnail_url, thumbnail_width, thumbnail_height, thumbnail_html_tag,
                     country_id, country_name, nb_views, nb_downloads,
                     keywords,
                     licenses,
                     licenses_details,
                     cat1,
                     cat_hierarchy)

        @nb_result = nb_result
        @id = id
        @title = title
        @creator_name = creator_name
        @creator_id = creator_id
        @media_type_id = media_type_id
        @thumbnail_url = thumbnail_url
        @thumbnail_width = thumbnail_width
        @thumbnail_height = thumbnail_height
        @thumbnail_html_tag = thumbnail_html_tag
        @country_id = country_id
        @country_name = country_name
        @nb_views = nb_views
        @nb_downloads = nb_downloads
        @keywords = keywords
        @licenses = licenses
        @licenses_details = licenses_details
        @cat1 = cat1
        @cat_hierarchy = cat_hierarchy
      end

      def self.from_json(json)
        json = JSON.parse json if json.is_a?(String)
        keywords = []
        json['keywords'].each do |k|
          keywords.push Fotoliapi::Models::KeywordFotolia.from_json(k)
        end
        licenses = []
        json['licenses'].each do |l|
          licenses.push Fotoliapi::Models::LicenseFotolia.from_json(l)
        end
        licenses_details = []
        json['licenses_details'].each do |s, ld|
          answer = Fotoliapi::Models::LicenseDetailsFotolia.from_json(ld)
          answer.size = s
          licenses_details.push answer
        end
        cat1 = nil
        cat1 = Fotoliapi::Models::CategoryFotolia.from_json(json['cat1']) if json['cat1'].present?
        cat_hierarchy = []
        json['cat1_hierarchy'].each do |ch|
          cat_hierarchy.push Fotoliapi::Models::CategoryFotolia.from_json(ch)
        end
        answer = Fotoliapi::Models::AnswerGetMediaDataFotolia.new(nil,
                                                                  json['id'], json['title'], json['creator_name'], json['creator_id'], json['media_type_id'],
                                                                  json['thumbnail_url'], json['thumbnail_width'], json['thumbnail_height'], json['thumbnail_html_tag'],
                                                                  json['country_id'], json['country_name'], json['nb_views'], json['nb_downloads'], keywords,
                                                                  licenses, licenses_details, cat1, cat_hierarchy)
        answer
      end
    end
  end
end
