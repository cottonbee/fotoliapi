module Fotoliapi
  module Models
    class AnswerShoppingCartTransferToLightboxFotolia
      attr_accessor :results

      def initialize(results = false)
        @results = results
      end

      def self.from_json(_json)
        answer = Fotoliapi::Models::AnswerShoppingCartTransferToLightboxFotolia.new true
        answer
      end
    end
  end
end
