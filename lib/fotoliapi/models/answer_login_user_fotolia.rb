module Fotoliapi
  module Models
    class AnswerLoginUserFotolia
      attr_accessor :session_token, :session_timestamp

      def initialize(session_token)
        @session_token = session_token
        @session_timestamp = Time.now
      end

      def self.from_json(json)
        json = JSON.parse json if json.is_a?(String)
        answer = Fotoliapi::Models::AnswerLoginUserFotolia.new(json['session_token'])
        answer
      end
    end
  end
end
