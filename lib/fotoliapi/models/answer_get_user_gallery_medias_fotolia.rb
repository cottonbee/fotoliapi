module Fotoliapi
  module Models
    class AnswerGetUserGalleryMediasFotolia
      attr_accessor :nb_results, :results

      def initialize(nb_results, results)
        @nb_results = nb_results
        @results = results
      end

      def self.from_json(json)
        json = JSON.parse json if json.is_a?(String)
        nb_results = json['nb_results']
        results = []
        json.each do |k, h|
          next unless (k != 'nb_results')
          gm = Fotoliapi::Models::GalleryMediaFotolia.from_json(h)
          gm.nb_result = k
          results.push gm
        end
        answer = Fotoliapi::Models::AnswerGetUserGalleryMediasFotolia.new(nb_results, results)
        answer
      end
    end
  end
end
