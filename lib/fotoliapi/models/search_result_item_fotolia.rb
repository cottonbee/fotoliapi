module Fotoliapi
  module Models
    class SearchResultItemFotolia
      attr_accessor :result_no, :id, :title, :creator_name, :creator_id,
                    :thumbnail_url, :thumbnail_html_tag, :thumbnail_width, :thumbnail_height,
                    :licenses, :nb_views, :nb_downloads, :thumbnail_400_url, :thumbnail_400_width,
                    :thumbnail_400_height

      def initialize(result_no = nil,
                     id = nil, title = nil, creator_name = nil, creator_id = nil,
                     thumbnail_url = nil, thumbnail_html_tag = nil, thumbnail_width = nil, thumbnail_height = nil,
                     licenses = nil, nb_views = nil, nb_downloads = nil, thumbnail_400_url = nil,
                     thumbnail_400_width = nil, thumbnail_400_height = nil)
        @result_no = result_no
        @id = id
        @title = title
        @creator_name = creator_name
        @creator_id = creator_id
        @thumbnail_url = thumbnail_url
        @thumbnail_html_tag = thumbnail_html_tag
        @thumbnail_width = thumbnail_width
        @thumbnail_height = thumbnail_height
        @thumbnail_400_url = thumbnail_400_url
        @thumbnail_400_width = thumbnail_400_width
        @thumbnail_400_height = thumbnail_400_height
        @licenses = licenses
        @nb_views = nb_views
        @nb_downloads = nb_downloads
      end

      def self.from_json(json)
        json = JSON.parse json if json.is_a?(String)
        answer = Fotoliapi::Models::SearchResultItemFotolia.new
        answer.id = json['id']
        answer.title = json['title']
        answer.creator_name = json['creator_name']
        answer.creator_id = json['creator_id']
        answer.thumbnail_url = json['thumbnail_url']
        answer.thumbnail_html_tag = json['thumbnail_html_tag']
        answer.thumbnail_width = json['thumbnail_width']
        answer.thumbnail_height = json['thumbnail_height']
        answer.thumbnail_400_url = json['thumbnail_400_url']
        answer.thumbnail_400_width = json['thumbnail_400_width']
        answer.thumbnail_400_height = json['thumbnail_400_height']
        answer.nb_views = json['nb_views']
        answer.nb_downloads = json['nb_downloads']

        answer.licenses = []
        # licenses
        json['licenses'].each do |k|
          l = Fotoliapi::Models::LicenseFotolia.new(k['name'], k['price'])
          answer.licenses.push l
        end
        answer
      end
    end
  end
end
