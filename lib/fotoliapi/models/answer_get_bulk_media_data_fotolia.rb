module Fotoliapi
  module Models
    class AnswerGetBulkMediaDataFotolia
      attr_accessor :results
      def initialize(results = nil)
        @results = results
      end

      def self.from_json(json)
        json = JSON.parse json if json.is_a?(String)
        results = []
        json.each do |k, h|
          media = Fotoliapi::Models::AnswerGetMediaDataFotolia.from_json(h)
          media.nb_result = k
          results.push media
        end
        answer = Fotoliapi::Models::AnswerGetBulkMediaDataFotolia.new(results)
        answer
      end
    end
  end
end
