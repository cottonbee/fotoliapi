module Fotoliapi
  module Models
    class AnswerGetUserAdvancedStatsFotolia
      attr_accessor :data
      def initialize(data)
        @data = data
      end

      def self.from_json(json)
        json = JSON.parse json if json.is_a?(String)
        answer = Fotoliapi::Models::AnswerGetUserAdvancedStatsFotolia.new(json)
        answer
      end
    end
  end
end
