module Fotoliapi
  module Models
    class SeasonalGalleriesItemFotolia
      attr_accessor :theme_id, :name, :thumbnail_url, :thumbnail_img, :galleries

      def initialize(theme_id, name, thumbnail_url, thumbnail_img, galleries)
        @theme_id = theme_id
        @name = name
        @thumbnail_url = thumbnail_url
        @thumbnail_img = thumbnail_img
        @galleries = galleries
      end

      def self.from_json(json)
        json = JSON.parse json if json.is_a?(String)
        galleries = [] # GalleryFotoliapi
        json['galleries'].each do |g|
          gallery = Fotoliapi::Models::GalleryFotolia.from_json(g)
          galleries.push gallery
        end
        answer = Fotoliapi::Models::SeasonalGalleriesItemFotolia.new(json['theme_id'], json['name'],
                                                                     json['thumbnail_url'], json['thumbnail_img'],
                                                                     galleries)
        answer
      end
    end
  end
end
