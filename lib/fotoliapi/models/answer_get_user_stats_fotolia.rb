module Fotoliapi
  module Models
    class AnswerGetUserStatsFotolia
      attr_accessor :nb_media_uploaded, :nb_media_purchased, :nb_media_sold, :ranking_absolute, :ranking_relative

      def initialize(nb_media_uploaded = nil, nb_media_purchased = nil, nb_media_sold = nil,
                     ranking_absolute = nil, ranking_relative = nil)
        @nb_media_uploaded = nb_media_uploaded
        @nb_media_purchased = nb_media_purchased
        @nb_media_sold = nb_media_sold
        @ranking_absolute = ranking_absolute
        @ranking_relative = ranking_relative
      end

      def self.from_json(json)
        json = JSON.parse json if json.is_a?(String)
        answer = Fotoliapi::Models::AnswerGetUserStatsFotolia.new(
          json['nb_media_uploaded'], json['nb_media_purchased'], json['nb_media_sold'],
          json['ranking_absolute'], json['ranking_relative'])
        answer
      end
    end
  end
end
