module Fotoliapi
  module Models
    class TagFotolia
      attr_accessor :tag, :popularity

      def initialize(tag, popularity)
        @tag = tag
        @popularity = popularity
      end

      def self.from_json(json)
        json = JSON.parse json if json.is_a?(String)
        answer = Fotoliapi::Models::TagFotolia.new(json['tag'], json['popularity'])
        answer
      end
    end
  end
end
