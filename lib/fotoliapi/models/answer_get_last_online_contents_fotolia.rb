module Fotoliapi
  module Models
    class AnswerGetLastOnlineContentsFotolia
      attr_accessor :results
      def initialize(results)
        @results = results
      end

      def self.from_json(json)
        json = JSON.parse json if json.is_a?(String)
        results = []
        json.each do |_k, h|
          results.push Fotoliapi::Models::AnswerGetMediaDataFotolia.from_json(h)
        end
        answer = Fotoliapi::Models::AnswerGetLastOnlineContentsFotolia.new(results)
        answer
      end
    end
  end
end
