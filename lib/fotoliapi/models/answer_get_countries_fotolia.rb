module Fotoliapi
  module Models
    class AnswerGetCountriesFotolia
      attr_accessor :results

      def initialize(results)
        @results = results
      end

      def self.from_json(json)
        json = JSON.parse json if json.is_a?(String)
        results = []
        json.each do |g|
          country = Fotoliapi::Models::CountryFotolia.from_json(g)
          results.push country
        end
        answer = Fotoliapi::Models::AnswerGetCountriesFotolia.new(results)
        answer
      end
    end
  end
end
