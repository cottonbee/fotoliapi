module Fotoliapi
  module Models
    class GalleryMediaFotolia
      attr_accessor :nb_result, :id, :title, :creator_name, :creator_id, :thumbnail_url, :thumbnail_html_tag, :thumbnail_width, :thumbnail_height, :licenses
      def initialize(nb_result = nil, id = nil, title = nil,
                     creator_name = nil, creator_id = nil,
                     thumbnail_url = nil, thumbnail_url_110 = nil, thumbnail_url_400 = nil, thumbnail_html_tag = nil, thumbnail_width = nil, thumbnail_height = nil,
                     licenses = nil)
        @nb_result = nb_result
        @id = id
        @title = title
        @creator_name = creator_name
        @creator_id = creator_id
        @thumbnail_url = thumbnail_url
        @thumbnail_url_110 = thumbnail_url_110
        @thumbnail_url_400 = thumbnail_url_400
        @thumbnail_html_tag = thumbnail_html_tag
        @thumbnail_width = thumbnail_width
        @thumbnail_height = thumbnail_height
        @licenses = licenses
      end

      def self.from_json(json)
        json = JSON.parse json if json.is_a?(String)
        licenses = []
        json['licenses'].each do |l|
          licenses.push Fotoliapi::Models::LicenseFotolia.from_json(l)
        end
        answer = Fotoliapi::Models::GalleryMediaFotolia.new(nil,
                                                            json['id'], json['title'],
                                                            json['creator_name'], json['creator_id'],
                                                            json['thumbnail_url_secure'], json['thumbnail_110_url_secure'], json['thumbnail_400_url_secure'], json['thumbnail_html_tag'], json['thumbnail_width'], json['thumbnail_height'],
                                                            licenses)
        answer
      end
    end
  end
end
