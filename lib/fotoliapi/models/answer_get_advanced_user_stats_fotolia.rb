module Fotolia
  module Models
    class AnswerGetAdvancedUserStatsFotolia
      attr_accessor :results # Hash<String, String>
      def initialize(results)
        super
        @results = results
      end
    end
  end
end
