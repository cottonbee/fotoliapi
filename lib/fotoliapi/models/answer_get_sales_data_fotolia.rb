module Fotoliapi
  module Models
    class AnswerGetSalesDataFotolia
      attr_accessor :results
      def initialize(results)
        @results = results
      end

      def self.from_json(json)
        json = JSON.parse json if json.is_a?(String)
        results = []
        answer = Fotoliapi::Models::AnswerGetSalesDataFotolia.new(results)
        answer
      end
    end
  end
end
