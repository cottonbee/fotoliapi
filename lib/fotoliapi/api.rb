
module Fotoliapi
  class Api
    def initialize(api_key)
      @api_key = api_key
      @req = Fotoliapi::Request.new(api_key)
    end

    def get_search_results(search_parameters, result_columns=[:nb_results,
                                                              :id,
                                                              :title,
                                                              :creator_name,
                                                              :creator_id,
                                                              :thumbnail_url,
                                                              :thumbnail_html_tag,
                                                              :thumbnail_width,
                                                              :thumbnail_height,
                                                              :affiliation_link,
                                                              :thumbnail_110_url,
                                                              :thumbnail_110_width,
                                                              :thumbnail_110_height,
                                                              :thumbnail_400_url,
                                                              :thumbnail_400_width,
                                                              :thumbnail_400_height,
                                                              :creation_date,
                                                              :media_type_id,
                                                              :flv_url,
                                                              :licenses])
      search_parameters[:secure_urls] = true
      params = { search_parameters: search_parameters, result_columns: result_columns }
      response = @req.send :get, 'search/getSearchResults', params
      service_response __method__, response
    end

    def get_categories_1(lang_id = Fotoliapi::Consts::LANGUAGE_ID_EN_US, id = 0)
      params = { language_id: lang_id, id: id }
      response = @req.send :get, 'search/getCategories1', params
      service_response __method__, response
    end

    def get_categories_2(lang_id = Fotoliapi::Consts::LANGUAGE_ID_EN_US, id = 0)
      params = { language_id: lang_id, id: id }
      response = @req.send :get, 'search/getCategories2', params
      service_response __method__, response
    end

    def get_tags(lang_id = Fotoliapi::Consts::LANGUAGE_ID_EN_US, type = 'Used')
      params = { language_id: lang_id, type: type }
      response = @req.send :get, 'search/getTags', params
      service_response __method__, response
    end

    def get_galleries(lang_id = Fotoliapi::Consts::LANGUAGE_ID_EN_US)
      params = { language_id: lang_id }
      response = @req.send :get, 'search/getGalleries', params
      service_response __method__, response
    end

    def get_seasonal_galleries(lang_id = Fotoliapi::Consts::LANGUAGE_ID_EN_US, thumbnail_size = 110, theme_id = nil)
      params = { language_id: lang_id, thumbnail_size: thumbnail_size }
      unless theme_id.nil?
        params = params.merge({theme_id: theme_id})
      end
      response = @req.send :get, 'search/getSeasonalGalleries', params
      service_response __method__, response
    end

    def get_countries(lang_id = Fotoliapi::Consts::LANGUAGE_ID_EN_US)
      params = { language_id: lang_id }
      response = @req.send :get, 'search/getCountries', params
      service_response __method__, response
    end

    def get_data
      response = @req.send :get, 'main/getData', nil
      service_response __method__, response
    end

    def test
      response = @req.send :get, 'main/test', nil
      service_response __method__, response
    end

    def get_media_data(id, thumbnail_size = 110, lang_id = Fotoliapi::Consts::LANGUAGE_ID_EN_US)
      params = { id: id, thumbnail_size: thumbnail_size, language_id: lang_id }
      response = @req.send :get, 'media/getMediaData', params
      service_response __method__, response
    end

    def get_bulk_media_data(ids, thumbnail_size = 100, lang_id = Fotoliapi::Consts::LANGUAGE_ID_EN_US)
      params = { ids: ids, thumbnail_size: thumbnail_size, language_id: lang_id }
      response = @req.send :get, 'media/getBulkMediaData', params
      service_response __method__, response
    end

    def get_media_galleries(id, lang_id = Fotoliapi::Consts::LANGUAGE_ID_EN_US, thumbnail_size = 110)
      params = { id: id, language_id: lang_id, thumbnail_size: thumbnail_size }
      response = @req.send :get, 'media/getMediaGalleries', params
      service_response __method__, response
    end

    def get_media(id, license_name, user_token)
      params = { id: id, license_name: license_name }
      auth_req = Fotoliapi::Request.new(@api_key, user_token)
      response = auth_req.send :get, 'media/getMedia', params
      service_response __method__, response
    end

    def get_media_comp(id)
      params = { id: id }
      response = @req.send :get, 'media/getMediaComp', params
      service_response __method__, response
    end

    def get_media_comp_700(id, user_token)
      params = { id: id }
      auth_req = Fotoliapi::Request.new(@api_key, user_token)
      response = auth_req.send :get, 'media/getMediaComp700', params
      service_response __method__, response
    end

    def login_user(login, pass)
      params = { login: login, pass: pass }
      response = @req.send :post, 'user/loginUser', params
      service_response __method__, response
    end

    def sign_up(properties)
      [:login, :password, :email, :language_id].each do|rp|
        fail Fotoliapi::ApiError, 'Missing required property: ' + rp.to_s unless properties.key? rp
      end
      response = @req.send :post, 'user/signup', properties
      service_response __method__, response
    end

    def get_user_data
      response = @req.send :get, 'user/getUserData', nil
      service_response __method__, response
    end

    def get_sales_data(sales_type = :all, offset = 0, limit = 50, id = nil, sales_day = nil)
      vsp = [:all, :subscription, :standard, :extended]
      fail Fotoliapi::ApiError, 'Undefined sales type ' + sales_type unless vsp.include? sales_type
      params = { sales_type: sales_type, offset: offset, limit: limit, id: id, sales_day: sales_day }
      response = @req.send :get, 'user/getSalesData', params
      service_response __method__, response
    end

    def get_user_advanced_stats(type, time_range, easy_date_period = nil, start_date = nil, end_date = nil)
      params = { type: type, time_range: time_range, easy_date_period: easy_date_period, start_date: start_date, end_date: end_date }
      response = @req.send :get, 'user/getUserAdvancedStats', params
      service_response __method__, response
    end

    def get_last_online_contents(offset, from_date, to_date, thumbnail_size, language_id = 2, limit = 50)
      params = { offset: offset, limit: limit, from_date: from_date, to_date: to_date, thumbnail_size: thumbnail_size, language_id: language_id }
      response = @req.send :get, 'user/getLastOnlineContents', params
      service_response __method__, response
    end

    def get_upload_folders(offset = nil, limit = 50)
      params = { offset: offset, limit: limit }
      response = @req.send :get, 'user/getUploadFolders', params
      service_response __method__, response
    end

    def get_upload_folder_file_ids(id, limit = 50, offset = nil)
      params = { id: id, limit: limit, offset: offset }
      response = @req.send :get, 'user/getUploadFolderFileIds', params
      service_response __method__, response
    end

    def get_last_uploaded_media(limit = 50, offset = nil)
      params = { offset: offset, limit: limit }
      response = @req.send :get, 'user/getLastUploadedMedia', params
      service_response __method__, response
    end

    def get_user_stats
      response = @req.send :get, 'user/getUserStats', nil
      service_response __method__, response
    end

    def delete_user_gallery(id)
      params = { id: id }
      response = @req.send :post, 'user/deleteUserGallery', params
      service_response __method__, response
    end

    def create_user_gallery(name)
      params = { name: name }
      response = @req.send :post, 'user/createUserGallery', params
      service_response __method__, response
    end

    def add_to_user_gallery(content_id, id)
      params = { content_id: content_id, id: id }
      response = @req.send :post, 'user/addToUserGallery', params
      service_response __method__, response
    end

    def remove_from_user_gallery(content_id, id)
      params = { content_id: content_id, id: id }
      response = @req.send :post, 'user/removeFromUserGallery', params
      service_response __method__, response
    end

    def get_user_gallery_medias(id, user_token, page = 1, nb_per_page = 32, thumbnail_size = 400, detail_level = 1)
      params = { page: page, nb_per_page: nb_per_page, thumbnail_size: thumbnail_size, id: id, detail_level: detail_level }
      auth_req = Fotoliapi::Request.new(@api_key, user_token)
      response = auth_req.send :get, 'user/getUserGalleryMedias', params
      service_response __method__, response
    end

    def get_user_galleries(user_token)
      auth_req = Fotoliapi::Request.new(@api_key, user_token)
      response = auth_req.send :get, 'user/getUserGalleries', nil
      service_response __method__, response
    end

    def move_up_media_in_user_gallery(content_id, id = '')
      params = { content_id: content_id, id: id }
      response = @req.send :post, 'user/moveUpMediaInUserGallery', params
      service_response __method__, response
    end

    def move_down_media_in_user_gallery(content_id, id = '')
      params = { content_id: content_id, id: id }
      response = @req.send :post, 'user/moveDownMediaInUserGallery', params
      service_response __method__, response
    end

    def move_media_to_top_in_user_gallery(content_id, id = '')
      params = { content_id: content_id, id: id }
      response = @req.send :post, 'user/moveMediaToTopInUserGallery', params
      service_response __method__, response
    end

    def shopping_cart_get_list
      response = @req.send :get, 'shoppingcart/getList', nil
      service_response __method__, response
    end

    def shopping_cart_clear
      response = @req.send :post, 'shoppingcart/clear', nil
      service_response __method__, response
    end

    def shopping_cart_transfer_to_lightbox(id)
      params = { id: id }
      response = @req.send :post, 'shoppingcart/transferToLightbox', params
      service_response __method__, response
    end

    def shopping_cart_add(id, license_name)
      params = { id: id, license_name: license_name }
      response = @req.send :post, 'shoppingcart/add', params
      service_response __method__, response
    end

    def shopping_cart_update(id, license_name)
      params = { id: id, license_name: license_name }
      response = @req.send :post, 'shoppingcart/update', params
      service_response __method__, response
    end

    def shopping_cart_remove(id)
      params = { id: id }
      response = @req.send :post, 'shoppingcart/remove', params
      service_response __method__, response
    end

    def test_get_error
      response = @req.send :get, 'error/getError', nil
      service_response __method__, response
    end

    private

    def service_response(method, response) # String, Faraday::Response
      st = response.status
      puts method

      if st == 401
        return Fotoliapi::Models::AnswerErrorFotolia.new(response.body, 401)
      end

      unless st.between?(200, 299)
        return Fotoliapi::Models::AnswerErrorFotolia.from_json(response.body)
      end

      answer =  case method

                  when :test_get_error then Fotoliapi::Models::AnswerErrorFotolia.from_json(response.body)
                  # Search
                  when :get_search_results then Fotoliapi::Models::AnswerGetSearchResultsFotolia.from_json(response.body)
                  when :get_categories_1 then Fotoliapi::Models::AnswerGetCategories1Fotolia.from_json(response.body)
                  when :get_categories_2 then Fotoliapi::Models::AnswerGetCategories2Fotolia.from_json(response.body)
                  when :get_tags then Fotoliapi::Models::AnswerGetTagsFotolia.from_json(response.body)
                  when :get_galleries then Fotoliapi::Models::AnswerGetGalleriesFotolia.from_json(response.body)
                  when :get_seasonal_galleries then Fotoliapi::Models::AnswerGetSeasonalGalleriesFotolia.from_json(response.body)
                  when :get_countries then Fotoliapi::Models::AnswerGetCountriesFotolia.from_json(response.body)

                  # Media
                  when :get_media_data then Fotoliapi::Models::AnswerGetMediaDataFotolia.from_json(response.body)
                  when :get_bulk_media_data then Fotoliapi::Models::AnswerGetBulkMediaDataFotolia.from_json(response.body)
                  when :get_media_galleries then Fotoliapi::Models::AnswerGetMediaGalleriesFotolia.from_json(response.body)
                  when :get_media then Fotoliapi::Models::AnswerGetMediaFotolia.from_json(response.body)
                  when :get_media_comp then Fotoliapi::Models::AnswerGetMediaCompFotolia.from_json(response.body)
                  when :get_media_comp_700 then Fotoliapi::Models::AnswerGetMediaCompFotolia.from_json(response.body)

                  # User
                  when :login_user then Fotoliapi::Models::AnswerLoginUserFotolia.from_json(response.body)
                  when :refresh_token then Fotoliapi::Models::AnswerRefreshTokenFotolia.from_json(response.body)
                  when :sign_up then Fotoliapi::Models::AnswerUserSignUpFotolia.from_json(response.body)
                  when :user_edit_profile then Fotoliapi::Models::AnswerUserEditProfileFotolia.from_json(response.body)
                  when :get_user_data then Fotoliapi::Models::AnswerGetUserDataFotolia.from_json(response.body)
                  when :get_sales_data then Fotoliapi::Models::AnswerGetSalesDataFotolia.from_json(response.body)
                  when :get_user_stats then Fotoliapi::Models::AnswerGetUserStatsFotolia.from_json(response.body)
                  when :get_user_galleries then Fotoliapi::Models::AnswerGetUserGalleriesFotolia.from_json(response.body)
                  when :get_user_gallery_medias then Fotoliapi::Models::AnswerGetUserGalleryMediasFotolia.from_json(response.body)
                  when :delete_user_gallery then Fotoliapi::Models::AnswerDeleteUserGalleryFotolia.from_json(response.body)
                  when :create_user_gallery then Fotoliapi::Models::AnswerCreateUserGalleryFotolia.from_json(response.body)
                  when :add_to_user_gallery then Fotoliapi::Models::AnswerAddToUserGalleryFotolia.from_json(response.body)
                  when :remove_from_user_gallery then Fotoliapi::Models::AnswerRemoveFromUserGalleryFotolia.from_json(response.body)
                  when :move_up_media_in_user_gallery then Fotoliapi::Models::AnswerMoveUpMediaInUserGalleryFotolia.from_json(response.body)
                  when :move_down_media_in_user_gallery then Fotoliapi::Models::AnswerMoveDownMediaInUserGalleryFotolia.from_json(response.body)
                  when :move_media_to_top_in_user_gallery then Fotoliapi::Models::AnswerMoveMediaToTopInUserGalleryFotolia.from_json(response.body)
                  when :get_user_advanced_stats then Fotoliapi::Models::AnswerGetUserAdvancedStatsFotolia.from_json(response.body)
                  when :get_last_online_contents then Fotoliapi::Models::AnswerGetLastOnlineContentsFotolia.from_json(response.body)
                  when :get_upload_folders then Fotoliapi::Models::AnswerGetUploadFoldersFotolia.from_json(response.body)
                  when :get_upload_folder_file_ids then Fotoliapi::Models::AnswerGetUploadFolderFileIdsFotolia.from_json(response.body)
                  when :upload_id_card then Fotoliapi::Models::AnswerUploadIdCardFotolia.from_json(response.body)
                  when :upload then Fotoliapi::Models::AnswerUploadFotolia.from_json(response.body)
                  when :get_last_uploaded_media then Fotoliapi::Models::AnswerGetLastUploadedMediaFotolia.from_json(response.body)

                  # Shopping Cart
                  when :shopping_cart_get_list then Fotoliapi::Models::AnswerShoppingCartGetListFotolia.from_json(response.body)
                  when :shopping_cart_add then Fotoliapi::Models::AnswerShoppingCartAddFotolia.from_json(response.body)
                  when :shopping_cart_update then Fotoliapi::Models::AnswerShoppingCartUpdateFotolia.from_json(response.body)
                  when :shopping_cart_remove then Fotoliapi::Models::AnswerShoppingCartRemoveFotolia.from_json(response.body)
                  when :shopping_cart_transfer_to_lightbox then Fotoliapi::Models::AnswerShoppingCartTransferToLightboxFotolia.from_json(response.body)
                  when :shopping_cart_clear then Fotoliapi::Models::AnswerShoppingCartClearFotolia.from_json(response.body)

                end

      answer
    end
  end
end
