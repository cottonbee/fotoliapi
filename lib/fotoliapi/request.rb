# require 'Faraday'

module Fotoliapi
  class Request

    def initialize(api_key, user_token = nil)
      url = Fotoliapi.request_uri
      version = Fotoliapi.rest_version
      @conn = Faraday.new(url: URI::join(url, version)) do |conn|
        conn.request :basic_auth, api_key, user_token unless api_key.nil?
        conn.request :url_encoded
        conn.response :logger
        conn.adapter :net_http # Faraday.default_adapter
      end
      super()
    end

    def send(method, url, params)
      fail StandardError, 'connection not configured' if @conn.nil?

      response = @conn.run_request(method, url, nil, nil) do|request|
        request.params.update(params) unless params.nil?
        yield(request) if block_given?
      end

      response
    end
  end
end
