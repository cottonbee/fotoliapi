require 'json'

require 'fotoliapi/version'
require 'fotoliapi/api'
require 'fotoliapi/consts'
require 'fotoliapi/request'
require 'fotoliapi/api_error'

require 'fotoliapi/models/answer_error_fotolia'

require 'fotoliapi/models/category_fotolia'
require 'fotoliapi/models/country_fotolia'
require 'fotoliapi/models/gallery_fotolia'
require 'fotoliapi/models/gallery_media_fotolia'
require 'fotoliapi/models/license_details_fotolia'
require 'fotoliapi/models/license_fotolia'
require 'fotoliapi/models/sales_data_fotolia'
require 'fotoliapi/models/search_result_item_fotolia'
require 'fotoliapi/models/seasonal_galleries_item_fotolia'
require 'fotoliapi/models/tag_fotolia'
require 'fotoliapi/models/keyword_fotolia'
require 'fotoliapi/models/upload_folder_fotolia'
require 'fotoliapi/models/user_gallery_fotolia'

require 'fotoliapi/models/answer_add_to_user_gallery_fotolia'
require 'fotoliapi/models/answer_create_user_gallery_fotolia'
require 'fotoliapi/models/answer_delete_user_gallery_fotolia'

require 'fotoliapi/models/answer_get_advanced_user_stats_fotolia'
require 'fotoliapi/models/answer_get_bulk_media_data_fotolia'
require 'fotoliapi/models/answer_get_search_results_fotolia'
require 'fotoliapi/models/answer_get_categories1_fotolia'
require 'fotoliapi/models/answer_get_categories2_fotolia'
require 'fotoliapi/models/answer_get_countries_fotolia'
require 'fotoliapi/models/answer_get_galleries_fotolia'
require 'fotoliapi/models/answer_get_last_online_contents_fotolia'
require 'fotoliapi/models/answer_get_last_uploaded_media_fotolia'
require 'fotoliapi/models/answer_get_media_data_fotolia'
require 'fotoliapi/models/answer_get_media_fotolia'
require 'fotoliapi/models/answer_get_media_comp_fotolia'
require 'fotoliapi/models/answer_get_media_galleries_fotolia'
require 'fotoliapi/models/answer_get_sales_data_fotolia'
require 'fotoliapi/models/answer_get_seasonal_galleries_fotolia'
require 'fotoliapi/models/answer_get_tags_fotolia'
require 'fotoliapi/models/answer_get_upload_folder_file_ids_fotolia'
require 'fotoliapi/models/answer_get_upload_folders_fotolia'
require 'fotoliapi/models/answer_get_user_advanced_stats_fotolia'
require 'fotoliapi/models/answer_get_user_data_fotolia'
require 'fotoliapi/models/answer_get_user_galleries_fotolia'
require 'fotoliapi/models/answer_get_user_gallery_medias_fotolia'
require 'fotoliapi/models/answer_get_user_stats_fotolia'
require 'fotoliapi/models/answer_login_user_fotolia'
require 'fotoliapi/models/answer_move_down_media_in_user_gallery_fotolia'
require 'fotoliapi/models/answer_move_media_to_top_in_user_gallery_fotolia'
require 'fotoliapi/models/answer_move_up_media_in_user_gallery_fotolia'
require 'fotoliapi/models/answer_refresh_token_fotolia'
require 'fotoliapi/models/answer_remove_from_user_gallery_fotolia'
require 'fotoliapi/models/answer_upload_fotolia'
require 'fotoliapi/models/answer_upload_id_card_fotolia'
require 'fotoliapi/models/answer_user_edit_profile_fotolia'
require 'fotoliapi/models/answer_user_sign_up_fotolia'
require 'fotoliapi/models/answer_shopping_cart_add_fotolia'
require 'fotoliapi/models/answer_shopping_cart_clear_fotolia'
require 'fotoliapi/models/answer_shopping_cart_get_list_fotolia'
require 'fotoliapi/models/answer_shopping_cart_remove_fotolia'
require 'fotoliapi/models/answer_shopping_cart_transfer_to_lightbox_fotolia'
require 'fotoliapi/models/answer_shopping_cart_update_fotolia'

module Fotoliapi
  extend Configuration

  define_setting :request_uri, Fotoliapi::Consts::FOTOLIA_REQUEST_URI
  define_setting :rest_version, Fotoliapi::Consts::FOTOLIA_REST_VERSION
  define_setting :token_timeout, Fotoliapi::Consts::TOKEN_TIMEOUT
  define_setting :api_connect_timeout, Fotoliapi::Consts::API_CONNECT_TIMEOUT
  define_setting :api_process_timeout, Fotoliapi::Consts::API_PROCESS_TIMEOUT
end
